<?php 
 
/**
 * AceLandSurveyingController Controller Class
 */
class AceLandSurveyingController extends Controller
{
    /**
     * Init method
     *
     * The init method is the default for controller classes. Whenever a controller
     * class is instantiated the init method will be called.
     */
    public function init()
    {
        $page = $this->load->model('pages')->getPage('name', 'ace-land-surveying');
 
        $data['title'] = $page['title'];
        $data['description'] = $page['description'];
 
        $view['header'] = $this->load->controller('header')->init($data);
        $view['footer'] = $this->load->controller('footer')->init();
        $view['content'] = $this->load->model('pages')->getPageContent('ace-land-surveying');
 
        $this->load->model('pages')->updatePageStatistics('ace-land-surveying');
 
        exit($this->load->view('common/content', $view));
    }
}